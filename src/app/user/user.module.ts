import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import { HomeForUserComponent } from './home-for-user/home-for-user.component';
@NgModule({
  imports: [
    CommonModule,RouterModule.forChild([
      {path:'',component:HomeForUserComponent}
    ])
  ],
  declarations: [ HomeForUserComponent]
})
export class UserModule { }
