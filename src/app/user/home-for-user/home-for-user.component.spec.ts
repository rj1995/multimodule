import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeForUserComponent } from './home-for-user.component';

describe('HomeForUserComponent', () => {
  let component: HomeForUserComponent;
  let fixture: ComponentFixture<HomeForUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeForUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeForUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
