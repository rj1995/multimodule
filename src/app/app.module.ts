import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { RouterModule } from '@angular/router';
import { UserModule } from './user/user.module';
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    BrowserModule, RouterModule.forRoot([
      { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
      { path: 'user', loadChildren: './user/user.module#UserModule' }
    ]
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
