import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeForAdminComponent } from './home-for-admin.component';

describe('HomeForAdminComponent', () => {
  let component: HomeForAdminComponent;
  let fixture: ComponentFixture<HomeForAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeForAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeForAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
