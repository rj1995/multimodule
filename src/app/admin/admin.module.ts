import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router'; 

import { HomeForAdminComponent } from './home-for-admin/home-for-admin.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path:'',component:HomeForAdminComponent}
    ])
  ],
  declarations: [
   
    HomeForAdminComponent
  ]
})
export class AdminModule { }
